package Clases;

import javax.print.DocFlavor;

/**
 * Created by Fernando Ambrosio on 07/07/2017.
 */
public class Proveedor {

    private int codProveedor;
    private String nom_Proveedor;
    private String direccion;
    private int telefono;

    public Proveedor() {
    }

    public Proveedor(int codProveedor, String nom_Proveedor, String direccion, int telefono) {
        this.codProveedor = codProveedor;
        this.nom_Proveedor = nom_Proveedor;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    public int getCodProveedor() {
        return codProveedor;
    }

    public void setCodProveedor(int codProveedor) {
        this.codProveedor = codProveedor;
    }

    public String getNom_Proveedor() {
        return nom_Proveedor;
    }

    public void setNom_Proveedor(String nom_Proveedor) {
        this.nom_Proveedor = nom_Proveedor;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }
}


