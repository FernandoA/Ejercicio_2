package Clases;

import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;


/**
 * Created by Fernando Ambrosio on 04/07/2017.
 */
public class Empresa {

    private String nom_Empresa;
    private List<Cliente> listaClientes;
    private List<Producto> listaProductos;
    private List<Proveedor> listaProveedor;


    public Empresa() {
    }

    public Empresa(String nom_Empresa) {
        this.nom_Empresa = nom_Empresa;
        listaClientes = new ArrayList<>();
        listaProductos = new ArrayList<>();
        listaProveedor = new ArrayList<>();

    }

    public String getNom_Empresa() {

        return nom_Empresa;
    }

    public void setNom_Empresa(String nom_Empresa)
    {
        this.nom_Empresa = nom_Empresa;
    }

    public List<Cliente> getListaCliente()
    {
        return  listaClientes;
    }

    public List<Producto> getListaProductos() {
        return listaProductos;
    }

    public List<Proveedor> getListaProveedor() {return listaProveedor;}



    public void ingresarProveedor(Proveedor proveedor){
        listaProveedor.add(proveedor);
    }

    public void ingressarCliente(Cliente cliente){

        listaClientes.add(cliente);
    }

    public void ingressarProducto(Producto producto){

        listaProductos.add(producto);
    }

    public float total()
    {
        float total1 = 0;
        for (Producto c:listaProductos)
        {
            total1 += c.getCosto();
        }
        return total1;
    }

}


