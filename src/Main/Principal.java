package Main;

import Clases.Cliente;
import Clases.Empresa;
import Clases.Producto;
import Clases.Proveedor;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by Fernando Ambrosio on 06/07/2017.
 */
public class Principal {

    public static void main(String[] args) throws Exception {

        Empresa empresa = null;
        Scanner scanner = new Scanner(System.in);
        boolean salir = false;
        int opcion;


        while (!salir)
        {
            System.out.println("Bienvenido al Sistema");
            System.out.println("Por favor escoja una Opcion");
            System.out.println("1. Ingresar el nombre de la empresa");
            System.out.println("2. Ingresar Proveedor");
            System.out.println("3. Mostrar Proveedor");
            System.out.println("4. Ingresar clientes");
            System.out.println("5. Mostrar clientes");
            System.out.println("6. Ingresar Productos");
            System.out.println("7. Mostrar Productos");
            System.out.println("8. Salir del Sistema");

            try
            {
                System.out.println("Seleccione una Opcion:");
                opcion = scanner.nextInt();
                switch (opcion)
                {
                    case 1:
                        System.out.println("Ingrese el nombre de la empresa:");
                        empresa = new Empresa(scanner.next());
                        break;

                    case 2:
                        System.out.println("Ingreso de Proveedor");
                        System.out.println("Codigo de Proveedor:");
                        Proveedor proveedor = new Proveedor();
                        proveedor.setCodProveedor(scanner.nextInt());
                        System.out.println("Nombre de Proveedor:");
                        proveedor.setNom_Proveedor(scanner.next());
                        System.out.println("Direccion Proveedor");
                        proveedor.setDireccion(scanner.next());
                        System.out.println("Telefono Proveedor");
                        proveedor.setTelefono(scanner.nextInt());
                        empresa.ingresarProveedor(proveedor);
                        break;

                    case 3:
                        System.out.println("Listado de Proveedores");
                        for (Proveedor proveedores: empresa.getListaProveedor())
                        {
                            System.out.printf("Codigo Proveedor:" + proveedores.getCodProveedor() + " Nombre Proveedor:" + proveedores.getNom_Proveedor() + " Direccion:" + proveedores.getDireccion() + " Telefono:" + proveedores.getTelefono());
                            System.out.println("\n");
                        }

                        break;

                    case 4:
                        System.out.println("Ingreso de Clientes");
                        System.out.println("Ingrese el NIT del cliente:");
                        Cliente cliente = new Cliente();
                        cliente.setNit(scanner.nextInt());
                        System.out.println("Ingrese el nombre del Cliente:");
                        cliente.setNombre(scanner.next());
                        System.out.println("Ingrese el apellido del Cliente");
                        cliente.setApellido(scanner.next());
                        System.out.println("Ingrese la edad del Cliente");
                        cliente.setEdad(scanner.nextInt());
                        empresa.ingressarCliente(cliente);
                        break;

                    case 5:
                        System.out.println("Listado de Clientes");
                        for (Cliente clientes:empresa.getListaCliente())
                        {
                            System.out.printf("Nit:" + clientes.getNit() + " Nombre:" + clientes.getNombre() + " Apellido:" + clientes.getApellido() + " Edad:" + clientes.getEdad() + " años");
                            System.out.println("\n");
                        }
                        break;

                    case 6:
                        System.out.println("Ingreso de  Productos");
                        System.out.println("Ingrese codigo de Producto");
                        Producto producto = new Producto();
                        producto.setCod_Producto(scanner.nextInt());
                        System.out.println("Ingrese Cliente");
                        producto.setCliente(scanner.next());
                        System.out.println("Ingrese Nombre del Producto");
                        producto.setNom_Producto((scanner.next()));
                        System.out.println("Valor del producto:");
                        producto.setCosto(scanner.nextFloat());
                        empresa.ingressarProducto(producto);
                        System.out.println("Operacion realizada Satisfactoriamente");
                        System.out.println("\n");
                        break;
                    case 7:
                        System.out.println("Listado de Productos");
                        for (Producto p : empresa.getListaProductos())
                        {
                            System.out.printf("Cliente:" + p.getCliente() + "Codigo Producto" + p.getCod_Producto() + " Nombre del Producto:" + p.getNom_Producto() + " Valor:Q" + p.getCosto() + "\n");
                        }
                        System.out.println("Valor Total de Productos: Q." + empresa.total());
                        System.out.println("\n");
                        break;

                    case 8:
                        salir = true;
                        break;
                }
            }
            catch (InputMismatchException e)
            {
                System.out.println("Debe insertar un numero");
                scanner.next();
            }

        }
    }
    }